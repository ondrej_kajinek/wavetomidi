#ifndef MIDI_FILE_H
#define MIDI_FILE_H

#include <fstream>
#include <string>
#include <vector>
#include "note_picker.h"

class MidiFile
{
	public:
		MidiFile(const std::string& filename);
		void write(const Notes& notes, double division);

	private:
		unsigned long check_output(unsigned long last_position, unsigned long bytes);

		std::ofstream m_file;
};

#endif /* MIDI_FILE_H */

