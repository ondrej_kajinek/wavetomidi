#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>
#include <iostream>


class BaseException: public std::exception
{
	public:
		BaseException(const std::string& message)
			: m_message(message)
		{}

		virtual const char* what() const throw()
		{
			return m_message.c_str();
		}

	private:
		std::string m_message;
};

class ArgsParserException: public BaseException
{
	public:
		ArgsParserException(const std::string& message)
			: BaseException(message)
		{}
};

class MissingArgument: public ArgsParserException
{
	public:
		MissingArgument(const std::string& arg_name)
			: ArgsParserException("Missing argument " + arg_name)
		{}
};

class MissingArgumentValue: public ArgsParserException
{
	public:
		MissingArgumentValue(const std::string& arg_name)
			: ArgsParserException("Missing value for argument " + arg_name)
		{}
};

class ShowHelp: public ArgsParserException
{
	public:
		ShowHelp()
			: ArgsParserException("")
		{}
};

class WtmException: public BaseException
{
	public:
		WtmException(const std::string& message)
			: BaseException(message)
		{}
};

class CannotReadFile: public WtmException
{
	public:
		CannotReadFile(const std::string& filename)
			: WtmException("Cannot open file " + filename)
		{}
};

class NoSndFileData: public WtmException
{
	public:
		NoSndFileData()
			: WtmException("No data found in sound file!")
		{}
};

class SndFileEOF: public WtmException
{
	public:
		SndFileEOF()
			: WtmException("End of snd_file")
		{}
};

class UnsupportedChannelCount: public WtmException
{
	public:
		UnsupportedChannelCount(int count)
			: WtmException(std::to_string(count) + " channels not supported, only mono or dual-channel stereo")
		{}
};

#endif /* EXCEPTION_H */

