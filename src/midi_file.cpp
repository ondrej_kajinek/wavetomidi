#include "midi_file.h"
#include "utils.h"

MidiFile::MidiFile(const std::string& filename)
	: m_file(filename, std::ofstream::out | std::ofstream::trunc)
{
}

void MidiFile::write(const Notes& notes, double division)
{
	// long div = (long)(0.5 * (double)sfinfo.samplerate / (double) args.step);
	if (!m_file.is_open()) {
		// TODO: raise
	}
	long last_position = m_file.tellp();
	// header
	m_file << "MThd"
			// header length
			<< midi_long(6)
			// 0 .. single track, 1 .. multiple tracks, 2 .. multiple songs
			<< midi_short(0)
			// tracks count
			<< midi_short(1)
			// division
			<< midi_short(division);

	last_position = check_output(last_position, 14);

	// track header
	m_file << "MTrk"
			<< midi_long(7 + 4 * notes.size());

	last_position = check_output(last_position, 8);
}

/*
	void
WAON_notes_output_midi (struct WAON_notes *notes,
		double div, char *filename)
{
	fprintf (stderr, "WAON_notes : n = %d\n", notes->n);
	fprintf (stderr, "filename : %s\n", filename);
	// file open
	int fd; // file descriptor of output midi file
	int flag_stdout;
	if (strncmp (filename, "-", strlen (filename)) == 0)
	{
		fd = fcntl(STDOUT_FILENO, F_DUPFD, 0);
		flag_stdout = 1;
	}
	else
	{
		fd = open (filename, O_RDWR| O_CREAT| O_TRUNC, S_IRUSR| S_IWUSR);
		flag_stdout = 0;
	}
	if (fd < 0)
	{
		fprintf (stderr, "cannot open %s\n", filename);
		exit (1);
	}

	// MIDI header
	int p_midi;
	int n_midi;
	p_midi = 0;
	n_midi = smf_header_fmt (fd, 0, 1, div);
	if (n_midi != 14)
	{
		fprintf (stderr, "Error during writing mid! %d (header)\n", p_midi);
		return;
	}
	p_midi += n_midi;

	int h_midi; // pointer of track header
	h_midi = p_midi; // pointer of track-head
	int nmidi = notes->n; // number of on-off events
	n_midi = smf_track_head (fd, (7+4*nmidi));
	if (n_midi != 8)
	{
		fprintf (stderr, "Error during writing mid! %d (track header)\n", p_midi);
		return;
	}
	p_midi += n_midi;

	// head of data
	int dh_midi; // pointer of data head
	dh_midi = p_midi;

	// tempo set
	n_midi = smf_tempo (fd, 500000); // 0.5 sec => 120 bpm for 4/4
	if (n_midi != 7)
	{
		fprintf (stderr, "Error during writing mid! %d (tempo)\n", p_midi);
		return;
	}
	p_midi += n_midi;

	// ch.0 prog. 0
	n_midi = smf_prog_change (fd, 0, 0);
	if (n_midi != 3)
	{
		fprintf (stderr, "Error during writing mid! %d (prog change)\n", p_midi);
		return;
	}
	p_midi += n_midi;

	int idt; // delta time
	int last_step = 0;
	int i;
	for (i = 0; i < notes->n; i ++)
	{
		// calc delta time
		if (i==0) idt = 0;
		else      idt = notes->step[i] - last_step;
		last_step = notes->step[i];

		// for check
		if (notes->event[i] == 1) // start note
		{
			n_midi = smf_note_on (fd, idt,
					notes->note[i],
					notes->vel[i],
					0);
		}
		else // stop note
		{
			n_midi = smf_note_off (fd, idt,
					notes->note[i],
					64, // default
					0);
		}
		if (n_midi < 4)
		{
			if (notes->event[i] == 1)
			{
				fprintf (stderr, "Error during writing mid! %d (note-on)\n"
						" idt = %d, note = %d, vel = %d, n_midi = %d\n",
						p_midi,
						idt, notes->note[i], notes->vel[i], n_midi);
			}
			else
			{
				fprintf (stderr, "Error during writing mid! %d (note-off)\n"
						" idt = %d, note = %d, vel = %d, n_midi = %d\n",
						p_midi,
						idt, notes->note[i], 64, n_midi);
			}
			// return;
		}
		p_midi += n_midi;
	}

	n_midi = smf_track_end (fd);
	if (n_midi != 4)
	{
		fprintf (stderr, "Error during writing mid! %d (track end)\n",
				p_midi);
		return;
	}
	p_midi += n_midi;

	if (flag_stdout == 0) // random-accessible file
	{
		// re-calculate # of data in track
		if (lseek (fd, h_midi, SEEK_SET) < 0)
		{
			fprintf (stderr, "Error during lseek %d (re-calc)\n", h_midi);
			return;
		}
		n_midi = smf_track_head (fd, (p_midi - dh_midi));
		if (n_midi != 8)
		{
			fprintf (stderr, "Error during write %d (re-calc)\n", p_midi);
			return;
		}
	}
	else // stdout
	{
		if ((7 + 4 * nmidi) != (p_midi - dh_midi))
			fprintf(stderr, "WaoN warning : data size seems to be different.\n");
	}

	close (fd);
}
*/

unsigned long MidiFile::check_output(unsigned long last_position, unsigned long bytes)
{
	long pointer = m_file.tellp();
	if (pointer - last_position != bytes) {
		// TODO: raise
		last_position = pointer;
	}
	return last_position;
}
