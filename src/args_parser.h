#ifndef CMD_PARSER_H
#define CMD_PARSER_H

#include <string>
#include "args.h"

class ArgsParser
{
	public:
		ArgsParser(const std::string& name);
		void help() const;
		void parse(Args& args, int argc, char* argv[]);

	private:
		void check_mandatory_args(const Args& args);
		void normalize_args(Args& args);

		std::string m_name;
};

#endif /* CMD_PARSER_H */

