#ifndef UTILS_H
#define UTILS_H

#include <string>

double hann(double x, unsigned int N);
std::string midi_long(long value);
std::string midi_short(short value);
double normalize_angle(double angle);

#endif /* UTILS_H */

