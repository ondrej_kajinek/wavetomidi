#ifndef SND_FILE_H
#define SND_FILE_H

#include <string>
#include "snd_file_handle.h"

class SndFile
{
	public:
		SndFile(const std::string& filename);

		bool dual_channel() const { return m_handle.info().channels == 2; }
		long read(double* left, double* right, unsigned int frames);
		unsigned int samplerate() const { return m_handle.info().samplerate; }
		// TODO: add methods for getting other info

	private:
		SndFileHandle m_handle;
};

#endif /* SND_FILE_H */

