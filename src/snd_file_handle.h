#ifndef SND_FILE_HANDLE_H
#define SND_FILE_HANDLE_H

#include <sndfile.h>
#include <string>

class SndFileHandle
{
	public:
		SndFileHandle(const std::string& filename);
		~SndFileHandle();

		SNDFILE* file() const { return m_file; }
		SF_INFO info() const { return m_info; }

	private:
		std::string full_format() const;
		std::string format_endianness() const;
		std::string format_subtype() const;
		std::string format_type() const;

		SF_INFO m_info;
		SNDFILE* m_file;
};

#endif /* SND_FILE_HANDLE_H */

