#ifndef BUFFER_H
#define BUFFER_H

#include <exception>
#include <memory>

/*
template <class T>
class buffer_iterator: public std::iterator<std::forward_iterator_tag, T>
{
	public:
		buffer_iterator(T* point)
			: m_pointer(point)
		{}

		buffer_iterator(const buffer_iterator<T>& rhs)
			: m_pointer(rhs.m_pointer)
		{}

		buffer_iterator& operator++() { ++m_pointer; return *this; }
		buffer_iterator operator++(int) { buffer_iterator<T> tmp(*this); operator++(); return tmp; }
		bool operator==(const buffer_iterator<T>& rhs) const { return m_pointer == rhs.m_pointer; }
		bool operator!=(const buffer_iterator<T>& rhs) const { return m_pointer != rhs.m_pointer; }
		T& operator*() { return *m_pointer; }

	private:
		T* m_pointer;
};
*/

template <class T>
class Buffer
{
	public:
		Buffer()
			: m_length(0),
			  m_buffer(nullptr)
		{}

		Buffer(unsigned int length)
			: m_length(length),
			  m_buffer((T*) malloc(sizeof(T) * length), free)
		{
			if (m_buffer.get() == nullptr) {
				throw std::bad_alloc();
			}
		}

		T* c_buffer() const { return m_buffer.get(); }
		unsigned int size() const { return m_length; }

		explicit operator bool() const { return m_buffer.get() != nullptr; }

		T& operator[](std::size_t idx) { return m_buffer.get()[idx]; }
		const T& operator[](std::size_t idx) const { return m_buffer.get()[idx]; }

		// buffer_iterator<T> begin() const { return buffer_iterator<T>(m_buffer); }
		// buffer_iterator<T> end() const { return buffer_iterator<T>(m_buffer + m_length); }

	private:
		unsigned int m_length;
		std::shared_ptr<T> m_buffer;
};


#endif /* BUFFER_H */

