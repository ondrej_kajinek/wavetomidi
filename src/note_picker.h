#ifndef NOTE_PICKER_H
#define NOTE_PICKER_H

#include <bitset>
#include <fftw3.h>
#include <vector>
#include "args.h"
#include "buffer.h"
#include "snd_file.h"

const size_t NOTES_COUNT = 128;
typedef std::vector<char> NoteFrame;
typedef std::vector<NoteFrame> Notes;
typedef Buffer<double> SoundBuffer;

class NotePicker
{
	public:
		NotePicker(const Args& args);
		Notes analyze();

	private:
		void read_window(SndFile& sound_file, unsigned int frames, unsigned int buffer_shift = 0);
		void shift_window();

		fftw_plan m_plan;
		Args m_args;
		SoundBuffer m_left_channel;
		SoundBuffer m_right_channel;
};

#endif /* NOTE_PICKER_H */

