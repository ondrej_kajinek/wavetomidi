#include <iostream>
#include "args_parser.h"
#include "exception.h"

ArgsParser::ArgsParser(const std::string& name)
	: m_name(name)
{
}

void ArgsParser::help() const
{
	std::cerr << "Usage: " << m_name << " [options]" << std::endl
					<< "Options:" << std::endl
					<< "\t-h,--help\t\tShow this help message" << std::endl
					<< "\t-i, --input\t\tMusic file to be analyzed" << std::endl
					<< "\t-w, --window\t\tWindow lenght for file processing" << std::endl
					<< "\t-s, --step\t\tWindow step" << std::endl;
}

void ArgsParser::parse(Args& args, int argc, char* argv[])
{
	for (int i = 1; i < argc; ++i) {
		std::string arg = argv[i];
		if (arg == "-h" || arg == "--help") {
			help();
			throw ShowHelp();
		}

		if (arg == "-i" || arg == "--input") {
			if (i + 1 < argc) {
				args.input_name = std::string(argv[++i]);
			}
			else {
				throw MissingArgumentValue(arg);
			}
		}
		if (arg == "-o" || arg == "--output") {
			if (i + 1 < argc) {
				args.output_name = std::string(argv[++i]);
			}
			else {
				throw MissingArgumentValue(arg);
			}
		}

		if (arg == "-p" || arg == "--padding") {
			if (i + 1 < argc) {
				args.padding_factor = std::stoi(argv[++i]);
			}
			else {
				throw MissingArgumentValue(arg);
			}
		}

		if (arg == "-s" || arg == "--step") {
			if (i + 1 < argc) {
				args.step = std::stoi(argv[++i]);
			}
			else {
				throw MissingArgumentValue(arg);
			}
		}

		if (arg == "-w" || arg == "--window") {
			if (i + 1 < argc) {
				args.window = std::stoi(argv[++i]);
			}
			else {
				throw MissingArgumentValue(arg);
			}
		}
	}
	check_mandatory_args(args);
	normalize_args(args);
}

void ArgsParser::check_mandatory_args(const Args& args)
{
	if (args.input_name == "") {
		throw MissingArgument("--input");
	}
}

void ArgsParser::normalize_args(Args& args)
{
	if (args.window == 0) {
		args.window = 1024;
	}
	if (args.step == 0 || args.step > args.window) {
		args.step = args.window / 4;
	}
}
