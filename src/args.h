#ifndef ARGS_H
#define ARGS_H

#include <string>

struct Args
{
	int padding_factor;
	int step;
	int window;
	std::string input_name;
	std::string output_name;

	Args()
		: padding_factor(1),
		  step(0),
		  window(0),
		  input_name(""),
		  output_name("")
	{}
};

#endif /* ARGS_H */

