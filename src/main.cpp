#include <fstream>
#include <iostream>
#include <vector>

#include "args_parser.h"
#include "exception.h"
#include "note_picker.h"
#include "snd_file.h"

int main(int argc, char* argv[])
{
	Args args;
	ArgsParser parser(argv[0]);
	try {
		parser.parse(args, argc, argv);
		NotePicker picker(args);
		Notes notes = picker.analyze();
		// TODO: when switching to midi output, add std::ofstream::binary
		std::fstream output(args.output_name, std::ofstream::out | std::ofstream::trunc);
		for (NoteFrame note_frame: notes) {
			for (unsigned int i = 0; i < NOTES_COUNT; ++i) {
				output << (note_frame[i] > 0);
				if (i && i % 12 == 0) {
					output << " ";
				}
			}
			output << std::endl;
		}
	}
	// ShowHelp inherits from ArgsParserException, must be earlier
	catch (ShowHelp& e) {
		// help shown
	}
	catch (ArgsParserException& e) {
		std::cerr << "Error when parsing arguments: " << e.what() << std::endl;
		parser.help();
	}
	catch (WtmException& e) {
		std::cerr << "Error during processing: " << e.what() << std::endl;
	}
	catch (std::exception& e) {
		std::cerr << "This exception should be caught!" << std::endl;
		std::cerr << e.what() << std::endl;
	}
	return EXIT_SUCCESS;
}

