#include <algorithm>
#include <fftw3.h>
#include <math.h>
#include "exception.h"
#include "note_picker.h"
#include "snd_file.h"
#include "utils.h"

#include <fstream>
#include <iostream>

NotePicker::NotePicker(const Args& args)
	: m_plan(nullptr),
	  m_args(args),
	  m_left_channel(),
	  m_right_channel()
{
}

Notes NotePicker::analyze()
{
	SndFile sound_file(m_args.input_name);
	unsigned int spatial_length = m_args.padding_factor * m_args.window;
	unsigned int frequency_length = spatial_length / 2 + 1;

	m_left_channel = SoundBuffer(spatial_length);
	if (sound_file.dual_channel()) {
		m_right_channel = SoundBuffer(spatial_length);
	}


	// initialize data for FFT, create a plan
	double* sound = (double*)fftw_alloc_real(spatial_length);
	for (unsigned int i = 0; i < spatial_length; ++i) {
		sound[i] = 0;
	}
	// only floor(n / 2 + 1) items are required, since freq[i] = conjugate(freq[n - i]) for real data
	fftw_complex* frequency = (fftw_complex*)fftw_alloc_complex(frequency_length);
	fftw_plan fft_plan = fftw_plan_dft_r2c_1d(spatial_length, sound, frequency, FFTW_MEASURE);
	SoundBuffer amplitude(frequency_length);
	SoundBuffer phase(frequency_length);
	SoundBuffer previous_phase(frequency_length);
	double frequency_step = sound_file.samplerate() / spatial_length;

	try {
		// read one frame to check if file is ok
		read_window(sound_file, 1);
	}
	catch (SndFileEOF& e) {
		throw NoSndFileData();
	}
	bool at_eof = false;
	bool first_iteration = true;
	try {
		// read the rest of window size
		read_window(sound_file, m_args.window - 1, 1);
	}
	catch (SndFileEOF& e) {
		at_eof = true;
	}
	Notes notes;
	while (!at_eof) {
		NoteFrame window_notes(NOTES_COUNT);
		for (int i = 0; i < m_args.window; ++i) {
			sound[i] = m_right_channel ? (0.5 * (m_right_channel[i] + m_left_channel[i])) : m_left_channel[i];
		}
		fftw_execute(fft_plan);
		for (unsigned int i = 0; i < frequency_length; ++i) {
			amplitude[i] = (frequency[i][0] * frequency[i][0] + frequency[i][1] * frequency[i][1]);
			previous_phase[i] = phase[i];
			// TODO: check atan
			phase[i] = std::atan(frequency[i][1] / frequency[i][0]);
		}

		// TODO: param
		int cutoff = -1;
		double threshold = pow(10.0, cutoff);
		double frequency;
		double velocity;
		// don't start at i = 0, since this is zero freq => no note
		for (unsigned int i = 1; i < frequency_length - 1; ++i) {
			// when amplitude is the same, we pick several neighbouring peaks
			if (amplitude[i] > threshold && amplitude[i] >= amplitude[i - 1] && amplitude[i] >= amplitude[i + 1]) {
				// branch prediction
				if (__builtin_expect(first_iteration == false, 1)) {
					frequency = i * frequency_step;
					velocity = amplitude[i];
				}
				else {
					double phase_diff = normalize_angle(
						phase[i] - previous_phase[i] - 2 * M_PI * m_args.step * i / (double)spatial_length
					);
					double kappa = spatial_length / (2 * M_PI * m_args.step) * phase_diff;
					frequency = (i + kappa) * frequency_step;
					double real_amplitude = 0.5 * amplitude[i] / (hann(kappa * m_args.window / (double)spatial_length, spatial_length));
					velocity = -127.0 * (std::log10(real_amplitude) - cutoff) / (double)cutoff;
				}
				int midi_note = 66.5 + 12 * std::log(frequency / 440.0) / std::log(2);
				if (midi_note > 0 and (unsigned int)midi_note < NOTES_COUNT) {
					window_notes[midi_note] = (char)(std::min(127.0, std::max(velocity, 0.0)));
				}
			}
		}

		notes.push_back(window_notes);
		first_iteration = false;

		// prepare data for next iteration
		shift_window();
		try {
			// read step frames
			read_window(sound_file, m_args.step, m_args.window - m_args.step);
		}
		catch (SndFileEOF& e) {
			at_eof = true;
			break;
		}
	}
	fftw_destroy_plan(fft_plan);
	fftw_free(sound);
	fftw_free(frequency);
	return notes;
}

void NotePicker::read_window(SndFile& sound_file, unsigned int frames, unsigned int buffer_shift)
{
	if (m_right_channel) {
		sound_file.read(m_left_channel.c_buffer() + buffer_shift, m_right_channel.c_buffer() + buffer_shift, frames);
	}
	else {
		sound_file.read(m_left_channel.c_buffer() + buffer_shift, nullptr, frames);
	}
}

void NotePicker::shift_window()
{
	for (int i = 0; i < (m_args.window - m_args.step); ++i) {
		m_left_channel[i] = m_left_channel[i + m_args.step];
		// right is nullptr for mono soundfile
		if (m_right_channel) {
			m_right_channel[i] = m_right_channel[i + m_args.step];
		}
	}
}

