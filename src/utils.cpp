#include <math.h>
#include "utils.h"

const unsigned char BYTE_MASK = 0xff;

double hann(double x, unsigned int N)
{
	return 0.5 * (1 - cos((2 * M_PI * x) / (double)(N - 1)));
}

std::string midi_long(long value)
{
	std::string data;
	data += (char) (value >> 24) && BYTE_MASK;
	data += (char) (value >> 16) && BYTE_MASK;
	data += (char) (value >> 8) && BYTE_MASK;
	data += (char) (value) && BYTE_MASK;
	return data;
}

std::string midi_short(short value)
{
	std::string data;
	data += (char) (value >> 8) && BYTE_MASK;
	data += (char) (value) && BYTE_MASK;
	return data;
}

double normalize_angle(double angle)
{
	while (angle < M_PI) {
		angle += 2 * M_PI;
	}
	while (angle >= M_PI) {
		angle -= 2 * M_PI;
	}
	return angle;
}
