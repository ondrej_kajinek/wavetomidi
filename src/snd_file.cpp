#include "buffer.h"
#include "exception.h"
#include "snd_file.h"

#include <iostream>

SndFile::SndFile(const std::string& filename)
	: m_handle(filename)
{
}

long SndFile::read(double* left, double* right, unsigned int frames)
{
	Buffer<double> buffer(frames * m_handle.info().channels);
	sf_count_t status = sf_readf_double(m_handle.file(), buffer.c_buffer(), (sf_count_t)frames);
	if (m_handle.info().channels == 1) {
		for (unsigned int i = 0; i < frames; ++i) {
			left[i] = buffer[i];
		}
	}
	// only two-channel stereo is supported
	else {
		for (unsigned int i = 0; i < frames; i ++)
		{
			left[i] = buffer[2 * i];
			right[i] = buffer[2 * i + 1];
		}
	}
	if ((long)status != frames) {
		throw SndFileEOF();
	}
	return (long)status;
}

