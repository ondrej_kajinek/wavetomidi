#include <iostream>
#include "exception.h"
#include "snd_file_handle.h"

SndFileHandle::SndFileHandle(const std::string& filename)
{
	m_file = sf_open(filename.c_str(), SFM_READ, &m_info);
#ifndef DNDEBUG
	// OndraK: double space intentional -- alignment
	std::cout << "Opened sndfile at " << m_file << std::endl;
#endif
	if (m_file == nullptr) {
		std::cerr << "Failed to open file " << filename << std::endl;
		throw CannotReadFile(filename);
	}
	else {
		if (!(1 < m_info.channels || m_info.channels > 2)) {
			std::cerr << "Only mono and dual-channel stereo is supported" << std::endl;
			throw UnsupportedChannelCount(m_info.channels);
		}
		std::cout << "Opened file " << filename << std::endl;
		std::cout << "\t" << "Frames: " << m_info.frames << std::endl;
		std::cout << "\t" << "Samplerate: " << m_info.samplerate << std::endl;
		std::cout << "\t" << "Channels: " << m_info.channels << std::endl;
		std::cout << "\t" << "Format: " << full_format() << std::endl;
		std::cout << "\t" << "Sectiions: " << m_info.sections << std::endl;
		std::cout << "\t" << "Seekable: " << m_info.seekable << std::endl;
	}
}

SndFileHandle::~SndFileHandle()
{
#ifndef DNDEBUG
	std::cout << "Closing sndfile at" << m_file << std::endl;
#endif
	if (m_file) {
		sf_close(m_file);
		// m_file = nullptr; unnecessary
	}
}

std::string SndFileHandle::full_format() const
{
	return format_type() + " (" + format_subtype() + "), " + format_endianness();
}

std::string SndFileHandle::format_endianness() const
{
	std::string endianness;
	switch (m_info.format & SF_FORMAT_ENDMASK) {
		case SF_ENDIAN_FILE:
			endianness = "Default file endian-ness";
			break;
		case SF_ENDIAN_LITTLE:
			endianness = "Force little endian-ness";
			break;
		case SF_ENDIAN_BIG:
			endianness = "Force big endian-ness";
			break;
		case SF_ENDIAN_CPU:
			endianness = "Force CPU endian-ness";
			break;
		default:
			endianness = "UNKNOWN ENDIANNESS";
			// TODO: raise: unknown endianness
			break;
	}
	return endianness;
}

std::string SndFileHandle::format_subtype() const
{
	std::string subtype;
	switch (m_info.format & SF_FORMAT_SUBMASK) {
		case SF_FORMAT_PCM_S8:
			subtype = "Signed 8 bit data";
			break;
		case SF_FORMAT_PCM_16:
			subtype = "Signed 16 bit data";
			break;
		case SF_FORMAT_PCM_24:
			subtype = "Signed 24 bit data";
			break;
		case SF_FORMAT_PCM_32:
			subtype = "Signed 32 bit data";
			break;
		case SF_FORMAT_PCM_U8:
			subtype = "Unsigned 8 bit data (WAV and RAW only)";
			break;
		case SF_FORMAT_FLOAT:
			subtype = "32 bit float data";
			break;
		case SF_FORMAT_DOUBLE:
			subtype = "64 bit float data";
			break;
		case SF_FORMAT_ULAW:
			subtype = "U-Law encoded";
			break;
		case SF_FORMAT_ALAW:
			subtype = "A-Law encoded";
			break;
		case SF_FORMAT_IMA_ADPCM:
			subtype = "IMA ADPCM";
			break;
		case SF_FORMAT_MS_ADPCM:
			subtype = "Microsoft ADPCM";
			break;
		case SF_FORMAT_GSM610:
			subtype = "GSM 6.10 encoding";
			break;
		case SF_FORMAT_VOX_ADPCM:
			subtype = "Oki Dialogic ADPCM encoding";
			break;
		case SF_FORMAT_G721_32:
			subtype = "32kbs G721 ADPCM encoding";
			break;
		case SF_FORMAT_G723_24:
			subtype = "24kbs G723 ADPCM encoding";
			break;
		case SF_FORMAT_G723_40:
			subtype = "40kbs G723 ADPCM encoding";
			break;
		case SF_FORMAT_DWVW_12:
			subtype = "12 bit Delta Width Variable Word encoding";
			break;
		case SF_FORMAT_DWVW_16:
			subtype = "16 bit Delta Width Variable Word encoding";
			break;
		case SF_FORMAT_DWVW_24:
			subtype = "24 bit Delta Width Variable Word encoding";
			break;
		case SF_FORMAT_DWVW_N:
			subtype = "N bit Delta Width Variable Word encoding";
			break;
		case SF_FORMAT_DPCM_8:
			subtype = "8 bit differential PCM (XI only";
			break;
		case SF_FORMAT_DPCM_16:
			subtype = "16 bit differential PCM (XI only";
			break;
		case SF_FORMAT_VORBIS:
			subtype = "Xiph Vorbis encoding";
			break;
		default:
			subtype = "UNKNOWN SUBTYPE";
			// TODO: raise: unknown format
			break;
	}
	return subtype;
}

std::string SndFileHandle::format_type() const
{
	std::string type;
	switch (m_info.format & SF_FORMAT_TYPEMASK) {
		case SF_FORMAT_WAV:
			type = "Microsoft WAV format (little endian)";
			break;
		case SF_FORMAT_AIFF:
			type = "Apple/SGI AIFF format (big endian)";
			break;
		case SF_FORMAT_AU:
			type = "Sun/NeXT AU format (big endian)";
			break;
		case SF_FORMAT_RAW:
			type = "RAW PCM data";
			break;
		case SF_FORMAT_PAF:
			type = "Ensoniq PARIS file format";
			break;
		case SF_FORMAT_SVX:
			type = "Amiga IFF / SVX8 / SV16 format";
			break;
		case SF_FORMAT_NIST:
			type = "Sphere NIST format";
			break;
		case SF_FORMAT_VOC:
			type = "VOC files";
			break;
		case SF_FORMAT_IRCAM:
			type = "Berkeley/IRCAM/CARL";
			break;
		case SF_FORMAT_W64:
			type = "Sonic Foundry's 64 bit RIFF/WAV";
			break;
		case SF_FORMAT_MAT4:
			type = "Matlab (tm) V4.2 / GNU Octave 2.0";
			break;
		case SF_FORMAT_MAT5:
			type = "Matlab (tm) V5.0 / GNU Octave 2.1";
			break;
		case SF_FORMAT_PVF:
			type = "Portable Voice Format";
			break;
		case SF_FORMAT_XI:
			type = "Fasttracker 2 Extended Instrument";
			break;
		case SF_FORMAT_HTK:
			type = "HMM Tool Kit format";
			break;
		case SF_FORMAT_SDS:
			type = "Midi Sample Dump Standard";
			break;
		case SF_FORMAT_AVR:
			type = "Audio Visual Research";
			break;
		case SF_FORMAT_WAVEX:
			type = "MS WAVE with WAVEFORMATEX";
			break;
		case SF_FORMAT_SD2:
			type = "Sound Designer 2";
			break;
		case SF_FORMAT_FLAC:
			type = "FLAC lossless file format";
			break;
		case SF_FORMAT_CAF:
			type = "Core Audio File format";
			break;
		case SF_FORMAT_WVE:
			type = "Psion WVE format";
			break;
		case SF_FORMAT_OGG:
			type = "Xiph OGG container";
			break;
		case SF_FORMAT_MPC2K:
			type = "Akai MPC 2000 sampler";
			break;
		case SF_FORMAT_RF64:
			type = "RF64 WAV file";
			break;
		default:
			type = "UNKNOWN FORMAT";
			// TODO: raise: unknown format
			break;
	}
	return type;
}
